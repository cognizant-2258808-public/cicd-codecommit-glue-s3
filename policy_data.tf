data "aws_iam_policy_document" "eco_iam_assume_policy_document_for_codepipeline" {
  statement {
    effect = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      identifiers = ["codepipeline.amazonaws.com"]
      type        = "Service"
    }

  }
}

data "aws_iam_policy_document" "eco_iam_assume_policy_document_for_glue" {
  statement {
    effect = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      identifiers = ["glue.amazonaws.com"]
      type        = "Service"
    }
  }
}

data "aws_iam_policy" "lambda_basic_execution_role_policy" {
  arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

data "aws_iam_policy_document" "eco_iam_assume_policy_document_for_lambda" {
  statement {
    principals {
      identifiers = ["lambda.amazonaws.com"]
      type        = "Service"
    }

    actions = ["sts:AssumeRole"]
  }
}

data "aws_iam_policy_document" "eco_iam_assume_policy_document_for_event_rule" {
  statement {
    effect = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      identifiers = ["events.amazonaws.com"]
      type        = "Service"
    }
  }
}

data "aws_iam_policy_document" "eco_iam_policy_document_for_s3" {
  statement {
    actions = [
      "s3:GetObject*",
      "s3:GetBucket*",
      "s3:List*",
      "s3:DeleteObject*",
      "s3:PutObject*",
      "s3:Abort*"
    ]

    effect = "Allow"

    resources = [
      aws_s3_bucket.eco_s3_bucket.arn,
      "${aws_s3_bucket.eco_s3_bucket.arn}/*"
    ]
  }
}

data "aws_iam_policy_document" "eco_iam_policy_document_for_codepipeline" {
  statement {
    actions = [
      "codecommit:GetBranch",
      "codecommit:GetCommit",
      "codecommit:UploadArchive",
      "codecommit:GetUploadArchiveStatus"
    ]

    resources = [aws_codecommit_repository.eco_codecommit_repository.arn]
    effect = "Allow"

  }
}

data "aws_iam_policy_document" "eco_iam_policy_document_for_lambda_invoke" {
  statement {
    effect = "Allow"
    actions = ["lambda:InvokeFunction"]
    resources = [aws_lambda_function.eco_lambda_function.arn]
  }
}

data "aws_iam_policy_document" "eco_iam_policy_document_for_cloudwatch" {
  statement {
    actions = ["cloudwatch:*", "logs:*"]
    resources = ["*"]
  }
}
data "aws_iam_policy_document" "eco_iam_policy_document_for_lambda" {

  statement {
    actions = ["glue:CreateJob", "glue:StartJobRun"]
    resources = ["*"]
  }

  statement {
    actions = ["iam:PassRole"]
    resources = [aws_iam_role.eco_iam_role_for_glue.arn]
  }

  statement {
    actions = [
      "codepipeline:PutJobSuccessResult",
      "codepipeline:PutJobFailureResult"
    ]

    resources = ["*"]
  }

  statement {
    actions = ["codecommit:GetFile"]
    resources = [aws_codecommit_repository.eco_codecommit_repository.arn]
  }
}

data "aws_iam_policy_document" "eco_iam_policy_document_for_kms" {

  statement {
    actions = ["kms:*"]

    resources = ["*"]

    principals {
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
      type        = "AWS"
    }
  }

  statement {
    actions = [
      "kms:Decrypt",
      "kms:DescribeKey",
      "kms:Encrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*"
    ]
    principals {
      identifiers = [
        aws_iam_role.eco_iam_role_for_codepipeline.arn,
        aws_iam_role.eco_iam_role_for_glue.arn,
        aws_iam_role.eco_iam_role_for_lambda.arn
      ]
      type        = "AWS"
    }
    resources = ["*"]
  }
}

data "aws_iam_policy_document" "eco_iam_policy_document_for_event_rule" {
  statement {
    actions = ["codepipeline:StartPipelineExecution"]
    resources = [aws_codepipeline.eco_codepipeline.arn]
  }
}
