variable "codecommit_repo_name" {
  description = "Name of the code commit repository"
  default = "MyTestRepository"
}

variable "s3_bucket_name" {
  default = "eco-test-s3-bucket"
}

variable "project_tag" {
  type = map(string)

  default = {
    Account: "eco_project"
    Project: "test_project"
  }
}

variable "glue_name" {
  default = "glue_name"
}