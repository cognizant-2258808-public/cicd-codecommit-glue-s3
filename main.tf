terraform {
  required_version = ">= 1.2.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "ap-southeast-1"
}

data "aws_caller_identity" "current" {}

data "archive_file" "lambda_function" {
  output_path = "lambda_etl_launch.zip"
  type        = "zip"
  source_dir = "lambda_etl_launch"
}

# Code commit repository
resource "aws_codecommit_repository" "eco_codecommit_repository" {
  repository_name = var.codecommit_repo_name
  description     = "This is the Sample App Repository"

  tags = var.project_tag
}

# s3 bucket
resource "aws_s3_bucket" "eco_s3_bucket" {
  bucket = var.s3_bucket_name
  force_destroy = true
  tags = var.project_tag
}

resource "aws_iam_role" "eco_iam_role_for_codepipeline" {
  name   = "role_for_codepipeline"
  assume_role_policy = data.aws_iam_policy_document.eco_iam_assume_policy_document_for_codepipeline.json

  tags = var.project_tag
}

resource "aws_iam_role_policy" "eco_iam_role_policy_for_codepipeline" {
  name = "role_policy_for_codepipeline"
  policy = data.aws_iam_policy_document.eco_iam_policy_document_for_codepipeline.json
  role   = aws_iam_role.eco_iam_role_for_codepipeline.id
}

resource "aws_kms_key" "eco_kms_key" {
  description             = "KMS key for codecommit"
  deletion_window_in_days = 10

  tags = var.project_tag
}

resource "aws_kms_key_policy" "eco_kms_key_policy" {
  key_id = aws_kms_key.eco_kms_key.id
  policy = data.aws_iam_policy_document.eco_iam_policy_document_for_kms.json

}

resource "aws_iam_role_policy" "eco_iam_role_policy_for_cloudwatch" {
  name = "role_policy_for_cloudwatch"
  policy = data.aws_iam_policy_document.eco_iam_policy_document_for_cloudwatch.json
  role   = aws_iam_role.eco_iam_role_for_glue.id
}

resource "aws_iam_role" "eco_iam_role_for_glue" {
  name   = "role_for_glue"
  assume_role_policy = data.aws_iam_policy_document.eco_iam_assume_policy_document_for_glue.json

  tags = var.project_tag
}

resource "aws_iam_role" "eco_iam_role_for_lambda" {
  name   = "role_for_lambda"
  assume_role_policy = data.aws_iam_policy_document.eco_iam_assume_policy_document_for_lambda.json

  tags = var.project_tag

}

resource "aws_iam_role_policy" "eco_iam_role_policy_for_lambda" {
  name = "policy_to_lambda"
  policy = data.aws_iam_policy_document.eco_iam_policy_document_for_lambda.json
  role   = aws_iam_role.eco_iam_role_for_lambda.id
}

resource "aws_iam_policy" "eco_iam_policy_for_s3" {
  name = "policy_to_access_s3"
  policy = data.aws_iam_policy_document.eco_iam_policy_document_for_s3.json

  tags = var.project_tag
}

resource "aws_iam_policy_attachment" "eco_iam_policy_attachment_for_s3" {
  name       = "attache_s3_to_pipeline_lambda_glue"
  policy_arn = aws_iam_policy.eco_iam_policy_for_s3.arn
  roles = [
    aws_iam_role.eco_iam_role_for_codepipeline.name,
    aws_iam_role.eco_iam_role_for_glue.name,
    aws_iam_role.eco_iam_role_for_lambda.name
  ]
}

resource "aws_lambda_function" "eco_lambda_function" {
  function_name = "lambda_etl_launch"
  filename = data.archive_file.lambda_function.output_path
  role          = aws_iam_role.eco_iam_role_for_lambda.arn
  handler = "lambda_etl_launch.lambda_handler"
  runtime = "python3.8"
  environment {
    variables = {
      REPOSITORY_NAME = aws_codecommit_repository.eco_codecommit_repository.repository_name
      FILENAME = "etl.py"
    }
  }

  tags = var.project_tag
}

resource "aws_iam_policy" "eco_iam_policy_for_lambda_invoke" {
  name = "policy_to_invoke_lambda_function"
  policy = data.aws_iam_policy_document.eco_iam_policy_document_for_lambda_invoke.json
}

resource "aws_iam_policy_attachment" "eco_iam_policy_attachment_for_lambda_invoke_to_pipeline" {
  name       = "attach_lambda_invoke_to_pipeline"
  policy_arn = aws_iam_policy.eco_iam_policy_for_lambda_invoke.arn
  roles = [aws_iam_role.eco_iam_role_for_codepipeline.name]
}

resource "aws_iam_policy_attachment" "eco_iam_policy_attachment_for_lambda_basic_exec_role_policy" {
  name       = "attach_lambda_basic_exec_role_policy"
  policy_arn = data.aws_iam_policy.lambda_basic_execution_role_policy.arn
  roles = [aws_iam_role.eco_iam_role_for_lambda.name]
}

resource "aws_codepipeline" "eco_codepipeline" {
  name     = "codepipeline-glue-deploy"
  role_arn = aws_iam_role.eco_iam_role_for_codepipeline.arn

  artifact_store {
    location = aws_s3_bucket.eco_s3_bucket.bucket
    type     = "S3"

    encryption_key {
      id   = aws_kms_key.eco_kms_key.id
      type = "KMS"
    }
  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeCommit"
      version          = "1"
      output_artifacts = ["source_output"]

      configuration = {
        RepositoryName = aws_codecommit_repository.eco_codecommit_repository.repository_name
        BranchName       = "main"
        PollForSourceChanges = false
      }
    }
  }

  stage {
    name = "Deploy"

    action {
      name            = "Deploy"
      category        = "Invoke"
      owner           = "AWS"
      provider        = "Lambda"
      input_artifacts = ["source_output"]
      version         = "1"

      configuration = {
        FunctionName     = aws_lambda_function.eco_lambda_function.function_name
        UserParameters   = jsonencode({
          "glue_job_name" = var.glue_name,
          "glue_role" = aws_iam_role.eco_iam_role_for_glue.name
        })
      }
    }
  }

  tags = var.project_tag
}

resource "aws_iam_role" "eco_iam_role_for_event_rule" {
  name = "role_for_event_rule"
  assume_role_policy = data.aws_iam_policy_document.eco_iam_assume_policy_document_for_event_rule.json
  path = "/"

  tags = var.project_tag
}

resource "aws_iam_role_policy" "eco_iam_role_policy_for_event_rule" {
  name = "role_policy_for_event_rule"
  policy = data.aws_iam_policy_document.eco_iam_policy_document_for_event_rule.json
  role   = aws_iam_role.eco_iam_role_for_event_rule.id
}

resource "aws_cloudwatch_event_rule" "eco_cloudwatch_event_rule" {
  name = "rule_for_codecommit_glue"
  event_pattern = jsonencode({
    source = ["aws.codecommit"]
    detail-type = ["CodeCommit Repository State Change"]
    resources = [aws_codecommit_repository.eco_codecommit_repository.arn]
    detail = {
      event = ["referenceCreated", "referenceUpdated"]
      referenceType = ["branch"]
      referenceName = ["main"]
    }
  })

  tags = var.project_tag
}

resource "aws_cloudwatch_event_target" "eco_cloudwatch_event_target" {
  arn  = aws_codepipeline.eco_codepipeline.arn
  rule = aws_cloudwatch_event_rule.eco_cloudwatch_event_rule.name
  role_arn = aws_iam_role.eco_iam_role_for_event_rule.arn

}